<?php

declare(strict_types=1);

namespace Grifix\Clock;

use DateTimeImmutable;
use Grifix\Date\DateTime\DateTime;

final class SystemClock implements ClockInterface
{

    /**
     * @inheritdoc
     */
    public function getCurrentTime(): \DateTimeImmutable
    {
        return new \DateTimeImmutable();
    }

    public function getCurrentDate(): DateTime
    {
        return new DateTime(new \DateTimeImmutable());
    }

    public function now(): DateTimeImmutable
    {
        return new \DateTimeImmutable();
    }
}
