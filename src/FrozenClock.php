<?php

declare(strict_types=1);

namespace Grifix\Clock;

use DateTimeImmutable;
use Grifix\Date\DateTime\DateTime;

final class FrozenClock implements ClockInterface
{
    private ?DateTimeImmutable $frozenTime = null;

    public function __construct()
    {
    }

    /**
     * @inheritdoc
     */
    public function getCurrentTime(): \DateTimeImmutable
    {
        if ($this->frozenTime) {
            return $this->frozenTime;
        }

        return new \DateTimeImmutable();
    }

    /**
     * @deprecated
     */
    public function freezeTime(DateTimeImmutable $date): void
    {
        $this->frozenTime = $date;
    }

    public function freezeDate(DateTime $date): void
    {
        $this->frozenTime = $date->getWrapped();
    }

    public function unfreezeTime(): void
    {
        $this->frozenTime = null;
    }

    public function getCurrentDate(): DateTime
    {
        return new DateTime($this->frozenTime ?? new DateTimeImmutable());
    }

    public function isFrozen(): bool
    {
        return $this->frozenTime !== null;
    }

    public function now(): DateTimeImmutable
    {
        return $this->frozenTime;
    }
}
