<?php

declare(strict_types=1);

namespace Grifix\Clock;

use Grifix\Date\DateTime\DateTime;
use Psr\Clock\ClockInterface as PrsClockInterface;

interface ClockInterface extends PrsClockInterface
{
    public const SECOND = 1e-6;
    public const MINUTE = 60;
    public const HOUR = self::MINUTE * 60;
    public const DAY = self::HOUR * 24;

    /**
     * @deprecated
     */
    public function getCurrentTime(): \DateTimeImmutable;

    public function getCurrentDate(): DateTime;
}
