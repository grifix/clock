<?php

declare(strict_types=1);

namespace Grifix\Clock\Tests;

use Grifix\Clock\FrozenClock;
use Grifix\Date\DateTime\DateTime;
use PHPUnit\Framework\TestCase;

final class FrozenClockTest extends TestCase
{
    public function testItWorks(): void
    {
        $clock = new FrozenClock();
        $frozenTime = new \DateTimeImmutable('2001-01-01');
        $clock->freezeTime($frozenTime);
        self::assertSame($frozenTime, $clock->getCurrentTime());
        self::assertSame($frozenTime, $clock->now());
        self::assertTrue($clock->isFrozen());
        $clock->unfreezeTime();
        self::assertNotEquals($frozenTime, $clock->getCurrentTime());
        self::assertFalse($clock->isFrozen());

    }

    public function testItWorksWithDate(): void
    {
        $clock = new FrozenClock();
        $frozenDate = DateTime::create(2001, 1, 1);
        $clock->freezeDate($frozenDate);
        self::assertTrue($frozenDate->isEqualTo($clock->getCurrentDate()));
        $clock->unfreezeTime();
        self::assertFalse($frozenDate->isEqualTo($clock->getCurrentDate()));
    }
}
